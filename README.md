# Image Compressor

Image compressor module, put the original images in original folder edit de default settings if you wish, run commands and you have compressed images.


# Requirements

  - Nodejs
  - npm

# Installation

  1. Clone the project to your local machine
  2. Put your original images in folder **/original**
  3. Run on terminal:
  ```sh
  $ npm install
  ```
  ```sh
  $ npm run compress
  ```

  4. All your images have a copy compressed on folder **/compressed**

Enjoy!!