const setting = require('./setup.json');

let webpack = {
    entry: './index.js',
    module: {
        rules: [
            {
                test: /\.(gif|png|jpe?g|svg)$/i,
                use: [
                  {
                    loader: 'file-loader',
                    options: {
                        name: './../compressed/[path][name].[ext]',
                    }
                  },
                  {
                        loader: 'image-webpack-loader',
                        options: {
                            mozjpeg: {
                                progressive: true,
                                quality: setting.jpeg.quality
                            },
                            pngquant: {
                                quality: [ (setting.png.quality / 100), 1],
                                speed: 4
                            },
                            gifsicle: {
                                interlaced: false,
                            }

                        }
                   }
                ]
            }            
        ]
    },
};

if ( setting.webp.enable ) {
    webpack.module.rules[0].use[1].options.webp = { quality: setting.webp.quality } ;
}

module.exports = webpack;